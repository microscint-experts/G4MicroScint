#include "HistogramStore.hh"
//#include "xAODRootAccess/tools/Message.h"
#include "TH1.h"
#include "TH1F.h"
#include "TString.h"
#include <iostream>
#include <vector>

/// Helper macro for checking histogram existanse in histogram list
#define HIST_DECLARED( HISTTYPE, NAME, EXP )                                                                    \
  {                                                                                                             \
    if (EXP) {                                                                                                  \
      printf("\nFATAL\n HistogramStore::%s Attempt to create second histogram named %s \n\n", HISTTYPE, NAME);  \
      abort();                                                                                                  \
    }                                                                                                           \
  }

/// Helper macro for checking histogram access in histogram list
#define HIST_ACCESSED( HISTTYPE, NAME, EXP )                                                                    \
  {                                                                                                             \
    if (EXP) {                                                                                                  \
      printf("\nFATAL\n HistogramStore::%s requested %s cannot be accessed. \n\n", HISTTYPE, NAME);             \
      abort();                                                                                                  \
    }                                                                                                           \
  }

// Constructor
HistogramStore::HistogramStore()
{
  fTotalHistsNumber = 0;
}

// Create and store TH1* histogram clone
void HistogramStore::createHist(TH1* hist)
{
  if(!hist) {
    printf("Failed to get hist.\n");
    return;
  }

  TH1F* h1 = dynamic_cast<TH1F*>(hist);
  TH2F* h2 = dynamic_cast<TH2F*>(hist);
  TH3F* h3 = dynamic_cast<TH3F*>(hist);
  TProfile* hP  = dynamic_cast<TProfile*>(hist);
  TProfile2D* hP2 = dynamic_cast<TProfile2D*>(hist);

  TString name = hist->GetName();
  if(h1) {
    HIST_DECLARED("createHist::TH1F", name.Data(), hasTH1F(name));
    TH1F* tmp = (TH1F*)h1->Clone();
    m_histoTH1F[name] = std::make_pair(fTotalHistsNumber,tmp);
    fTotalHistsNumber++;
  }
  if(h2) {
    HIST_DECLARED("createHist::TH2F", name.Data(), hasTH2F(name));
    TH2F* tmp = (TH2F*)h2->Clone();
    m_histoTH2F[name] = std::make_pair(fTotalHistsNumber,tmp);
    fTotalHistsNumber++;
  }
  if(h3) {
    HIST_DECLARED("createHist::TH3F", name.Data(), hasTH3F(name));
    TH3F* tmp = (TH3F*)h3->Clone();
    m_histoTH3F[name] = std::make_pair(fTotalHistsNumber,tmp);
    fTotalHistsNumber++;
  }

  if(hP || hP2) {
    printf("HistogramStore::createHist() You've got to update this function to work eith profiles in propper way.\n");
    abort();
  }
}

// Create and store TH1F histogram
void HistogramStore::createTH1F(TString name, int Nbins, double xmin, double xmax, TString title) 
{
  HIST_DECLARED("createHistoTH1F", name.Data(), hasTH1F(name));
  m_histoTH1F[name] = std::make_pair(fTotalHistsNumber,new TH1F(name,title,Nbins,xmin,xmax));
  fTotalHistsNumber++;
  // m_histoTH1F[name]->Sumw2();
}

// Create and Store TH1F histogram
void HistogramStore::createTH1F(TString name, const std::vector<double> &bins, TString title) 
{
  HIST_DECLARED("createHistoTH1F", name.Data(), hasTH1F(name));
  m_histoTH1F[name] = std::make_pair(fTotalHistsNumber,new TH1F(name,title,-1+bins.size(),&bins[0]));
  fTotalHistsNumber++;
  // m_histoTH1F[name]->Sumw2();
}

// Create and store TH2F histogram
void HistogramStore::createTH2F(TString name, int NbinsX, double xmin, double xmax, int NBinsY, double ymin, double ymax, TString title) 
{
  HIST_DECLARED("createHistoTH2F", name.Data(), hasTH2F(name));
  m_histoTH2F[name] = std::make_pair(fTotalHistsNumber,new TH2F(name, title, NbinsX, xmin, xmax, NBinsY, ymin, ymax));
  fTotalHistsNumber++;
  // m_histoTH2F[name]->Sumw2();
}

// Create and store TH2F histogram
void HistogramStore::createTH2F(TString name, const std::vector<double> &xbins, const std::vector<double> &ybins, TString title) 
{
  HIST_DECLARED("createHistoTH2F", name.Data(), hasTH2F(name));
  m_histoTH2F[name] = std::make_pair(fTotalHistsNumber,new TH2F(name, title, xbins.size() - 1, &xbins[0],  ybins.size() - 1, &ybins[0]));
  fTotalHistsNumber++;
  // m_histoTH2F[name]->Sumw2();
}

// Create and store TH2F histogram
void HistogramStore::createTH2F(TString name, const std::vector<double> &xbins, int NBinsY, double ymin, double ymax, TString title) 
{
  HIST_DECLARED("createHistoTH2F", name.Data(), hasTH2F(name));
  m_histoTH2F[name] = std::make_pair(fTotalHistsNumber,new TH2F(name, title, xbins.size() - 1, &xbins[0],  NBinsY, ymin, ymax));
  fTotalHistsNumber++;
  // m_histoTH2F[name]->Sumw2();
}

// Create and store TH3F histogram
void HistogramStore::createTH3F(TString name, int NbinsX, double xmin, double xmax, int NBinsY, double ymin, double ymax, 
                      int NbinsZ, double zmin, double zmax, TString title) 
{
  HIST_DECLARED("createHistoTH3F", name.Data(), hasTH3F(name));
  m_histoTH3F[name] = std::make_pair(fTotalHistsNumber,new TH3F(name, title, NbinsX, xmin, xmax, NBinsY, ymin, ymax, NbinsZ, zmin, zmax));
  fTotalHistsNumber++;
  // m_histoTH3F[name]->Sumw2();
}

// Create and store TH3F histogram
void HistogramStore::createTH3F(TString name, const std::vector<double> &xbins, const std::vector<double> &ybins, 
                    const std::vector<double> &zbins, TString title) 
{
  HIST_DECLARED("createHistoTH3F", name.Data(), hasTH3F(name));
  m_histoTH3F[name] = std::make_pair(fTotalHistsNumber,new TH3F(name, title, xbins.size() - 1, &xbins[0],  ybins.size() - 1, &ybins[0], zbins.size() - 1, &zbins[0]));
  fTotalHistsNumber++;
  // m_histoTH3F[name]->Sumw2();
}

// Create and store TProfile histogram
void HistogramStore::createTProfile(TString name, int NbinsX, double xmin, double xmax, TString title) 
{
  HIST_DECLARED("createHistoTProfile", name.Data(), hasTH1F(name));
  m_histoTProfile[name] = std::make_pair(fTotalHistsNumber,new TProfile(name, title, NbinsX, xmin, xmax));
  fTotalHistsNumber++;
  // m_histoTProfile[name]->Sumw2();
}

// Create and store TProfile histogram
void HistogramStore::createTProfile(TString name, const std::vector<double> &xbins, TString title) 
{
  HIST_DECLARED("createHistoTProfile", name.Data(), hasTProfile(name));
  m_histoTProfile[name] = std::make_pair(fTotalHistsNumber,new TProfile(name, title, xbins.size() - 1, &xbins[0]));
  fTotalHistsNumber++;
  // m_histoTProfile[name]->Sumw2();
}

// Create and store TProfile histogram
void HistogramStore::createTProfile(TString name, int nbins, const double xlow, const double xup, const double ylow, const double yup, TString title) 
{
  HIST_DECLARED("createHistoTProfile", name.Data(), hasTProfile(name));
  m_histoTProfile[name] = std::make_pair(fTotalHistsNumber,new TProfile(name, title, nbins, xlow, xup, ylow, yup));
  fTotalHistsNumber++;
  // m_histoTProfile[name]->Sumw2();
}


// Retrieve a TH1F histogram from the internal store
TH1F* HistogramStore::getTH1F(TString name)
{
  HIST_ACCESSED("getTH1F", name.Data(), !hasTH1F(name));
  return m_histoTH1F[name].second;
}

// Retrieve a TH2F histogram from the internal store
TH2F* HistogramStore::getTH2F(TString name)
{
  HIST_ACCESSED("getTH2F", name.Data(), !hasTH2F(name));
  return m_histoTH2F[name].second;
}

// Retrieve a TH3F histogram from the internal store
TH3F* HistogramStore::getTH3F(TString name)
{
  HIST_ACCESSED("getTH3F", name.Data(), !hasTH3F(name));
  return m_histoTH3F[name].second;
}

// Retrieve a TH2F histogram from the internal store
TProfile* HistogramStore::getTProfile(TString name)
{
  HIST_ACCESSED("getTProfile", name.Data(), !hasTProfile(name));
  return m_histoTProfile[name].second;
}


// Return vector of all histograms in internal store
std::vector<TH1*> HistogramStore::getListOfHistograms() const
{
  std::vector<std::pair<int,TH1*>> sortBuffer;
  for (auto itr : m_histoTH1F) 
    sortBuffer.push_back(itr.second);
  for (auto itr : m_histoTH2F) 
    sortBuffer.push_back(itr.second);
  for (auto itr : m_histoTH3F) 
    sortBuffer.push_back(itr.second);
  for (auto itr : m_histoTProfile) 
    sortBuffer.push_back(itr.second);

  std::sort(sortBuffer.begin(),sortBuffer.end());


  std::vector<TH1*> allHistos;
  // an iterator of a map is pair<int,valueType>
  for (auto itr : sortBuffer ) {
    allHistos.push_back(itr.second);
  }

  return allHistos;
}
