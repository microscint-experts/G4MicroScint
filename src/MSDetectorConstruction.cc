#include "MSDetectorConstruction.hh"
#include "MSMaterials.hh"
#include "MSPhotonDetSD.hh"

#include "G4ios.hh"
#include "globals.hh"

#include "G4Box.hh"
#include "G4Tubs.hh"

#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"

#include "G4OpBoundaryProcess.hh"
#include "G4LogicalSkinSurface.hh"
#include "G4LogicalBorderSurface.hh"
#include "G4OpticalSurface.hh"

#include "G4Material.hh"
#include "G4NistManager.hh"

#include "G4GeometryManager.hh"
#include "G4SolidStore.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4MultiFunctionalDetector.hh"

#include "G4RunManager.hh"

#include "G4UserLimits.hh"
#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"

#include "G4VisAttributes.hh"

#include "G4String.hh"
#include <string>
#include <iostream>

MSDetectorConstruction::MSDetectorConstruction()
  : fMaterials(NULL), fLogVolumeWorld(NULL), fPhysVolumeWorld(NULL)
{

  fMSplateX  = 8*cm; 
  fMSplateY  = 25*mm;
  fMSplateZ  = 1.96*mm;
  fPlateCoverType = CoverType::NOCOVER; //CoverType::TAPE;

  fSiPMXsize = 0.1*mm;
  fSiPMYsize = 1.*mm;
  fSiPMZsize = 1.*mm;

  fCollimatorOffset = 0.5*cm; // 0.5*cm;
  fCollimatorZ     = 2.5*cm;
  fCollimatorRIn   = 0.5*mm; // 0.5*mm;
  fCollimatorROut  = 2.5*mm; // 2.5*mm;
  fCollimatorPositioning.set(-3.5*cm, 0.0*mm, fMSplateZ/2+fCollimatorOffset+fCollimatorZ/2);

  fMSScintWall    = 0.5*mm;
  fMSscintY       = 0.5*mm;
  fMSscintZ       = 1.0*mm;
  fMSscintZshift  = -0.01*mm;
  fMSScintGap     = 0.5*mm;
  fSurfaceRoughness = 0.78;

  fNScLanes       = 4;

  vPhysVolumesScint.clear();
  vPhysVolSiPM.clear();
  vPhysVolSupports.clear();

  UpdateGeometryParameters();
}

MSDetectorConstruction::~MSDetectorConstruction()
{
  if (fMaterials)         delete fMaterials;
  vPhysVolumesScint.clear();
  vPhysVolSiPM.clear();
  vPhysVolSupports.clear();
}

void MSDetectorConstruction::UpdateGeometryParameters()
{
  fWorldSizeX = fMSplateX + fSiPMZsize + 10*cm;
  fWorldSizeY = fMSplateY + 10*cm;
  fWorldSizeZ = fMSplateZ + 10*cm;

  fMSscintX   = fMSplateX - 2.*fMSScintWall;

  fSiPMPositioning.setX(-1.*(fMSplateX+fSiPMXsize)/2.);

  vPhysVolumesScint.clear();
  vPhysVolSiPM.clear();
  vPhysVolSupports.clear();
}

G4VPhysicalVolume* MSDetectorConstruction::Construct()
{
  if (fPhysVolumeWorld) {
     G4GeometryManager::GetInstance()->OpenGeometry();
     G4PhysicalVolumeStore::GetInstance()->Clean();
     G4LogicalVolumeStore::GetInstance()->Clean();
     G4SolidStore::GetInstance()->Clean();
     G4LogicalSkinSurface::CleanSurfaceTable();
     G4LogicalBorderSurface::CleanSurfaceTable();
  }

  fMaterials = MSMaterials::GetInstance();

  UpdateGeometryParameters();

  return ConstructDetector();
}

G4VPhysicalVolume* MSDetectorConstruction::ConstructDetector()
{
  //--------------------------------------------------
  // World
  //--------------------------------------------------
  G4String worldName = "World";
  G4VSolid* solidWorld = new G4Box(worldName, fWorldSizeX/2., fWorldSizeY/2., fWorldSizeZ/2.);
  fLogVolumeWorld = new G4LogicalVolume(solidWorld, FindMaterial("G4_AIR"), worldName);
  fPhysVolumeWorld = new G4PVPlacement(0, G4ThreeVector(), fLogVolumeWorld, worldName, 0, false, 0);
  vPhysVolSupports.push_back(fPhysVolumeWorld);

  fLogVolumeWorld->SetVisAttributes (G4VisAttributes::GetInvisible());

  //--------------------------------------------------
  // Plastic plate
  //--------------------------------------------------
  G4String plateName = "Plate";
  G4VSolid* solidPlate = new G4Box(plateName, fMSplateX/2., fMSplateY/2., fMSplateZ/2.);
  G4LogicalVolume*   logVolumePlate = new G4LogicalVolume(solidPlate, FindMaterial("HBP"), plateName);  // HBP
  G4VPhysicalVolume* physVolumePlate = new G4PVPlacement(0, G4ThreeVector(), logVolumePlate, plateName, fLogVolumeWorld, false, 0);
  vPhysVolSupports.push_back(physVolumePlate);
  CreatePlateSurface(physVolumePlate);

  //--------------------------------------------------
  // Scintillator
  //--------------------------------------------------
  CrateScintillator(logVolumePlate);
  CreateScintSurface(physVolumePlate);

  //--------------------------------------------------
  // SiPM
  //--------------------------------------------------
  CrateSiPM();

  //--------------------------------------------------
  // Collimator
  //--------------------------------------------------
  CrateCollimator();

  //--------------------------------------------------
  // End of Construction
  //--------------------------------------------------

  return fPhysVolumeWorld;
}

void MSDetectorConstruction::CreatePlateSurface(G4VPhysicalVolume* physVolumePlate)
{
  if(fPlateCoverType == CoverType::TAPE) {
   
    G4OpticalSurface* opHBPPlateSurface = new G4OpticalSurface("HBPPlateSurface");
    new G4LogicalBorderSurface("HBPPlateSurface",physVolumePlate,fPhysVolumeWorld,opHBPPlateSurface);   
    opHBPPlateSurface->SetType(dielectric_dielectric);
    opHBPPlateSurface->SetModel(glisur);
    opHBPPlateSurface->SetFinish(ground);
    opHBPPlateSurface->SetPolish(0.0);
    // opHBPPlateSurface->SetModel(unified);
    // opHBPPlateSurface->SetSigmaAlpha(0.1);

    /*
    const G4int nEntries = 2;
    G4double PhotonEnergy[nEntries] = {
      1.54986*eV,  // 800 nanomete
      5.90424*eV   // 210 nanometer
    };
    G4double Reflectivity[nEntries]        = {0.0, 0.0}; //0.5  
    G4double Efficiency[nEntries]          = {0.8, 0.8}; //0.8
    G4double specularlobe[nEntries]        = {0.3, 0.3}; //0.3        
    G4double specularspike[nEntries]       = {0.2, 0.2}; //0.2
    G4double backscatter[nEntries]         = {0.1, 0.1}; //0.1

    G4MaterialPropertiesTable* opHBPPlateSurfaceProperty = new G4MaterialPropertiesTable();
    opHBPPlateSurfaceProperty->AddProperty("REFLECTIVITY",  PhotonEnergy, Reflectivity, nEntries);
    opHBPPlateSurfaceProperty->AddProperty("EFFICIENCY",   PhotonEnergy, Efficiency,     nEntries);
    opHBPPlateSurfaceProperty->AddProperty("SPECULARLOBECONSTANT", PhotonEnergy, specularlobe, nEntries);
    opHBPPlateSurfaceProperty->AddProperty("SPECULARSPIKECONSTANT",PhotonEnergy, specularspike, nEntries);
    opHBPPlateSurfaceProperty->AddProperty("BACKSCATTERCONSTANT",PhotonEnergy, backscatter, nEntries);
    opHBPPlateSurface->SetMaterialPropertiesTable(opHBPPlateSurfaceProperty);
    //*/
  } else
  if(fPlateCoverType == CoverType::NOCOVER) {
    G4OpticalSurface* opHBPPlateSurface = new G4OpticalSurface("HBPPlateSurface");
    opHBPPlateSurface->SetType(dielectric_dielectric);
    opHBPPlateSurface->SetFinish(polished); // polished ground groundbackpainted
    opHBPPlateSurface->SetModel(unified);    // glisur unified
    new G4LogicalBorderSurface("HBPPlateSurfaceIn",physVolumePlate,fPhysVolumeWorld,opHBPPlateSurface);
    new G4LogicalBorderSurface("HBPPlateSurfaceOut",fPhysVolumeWorld,physVolumePlate,opHBPPlateSurface);
  } else {
    std::ostringstream o;
    o << "fPlateCoverType = " << fPlateCoverType << " That is forbitten!";
    G4Exception("MSDetectorConstruction::CreatePlateSurface","", FatalException, o.str().c_str());
  }
}

void MSDetectorConstruction::CrateScintillator(G4LogicalVolume* aParentLogicalVolume, G4String aTag)
{
  G4Material* scintMaterial = FindMaterial("EJ309");
  G4Material* scintMaterial_Air = FindMaterial("G4_AIR");
  G4String bigScName   = aTag + "MCh_";
  G4String smallScName = aTag + "SCh_";

  G4double positionY = 0*mm;

  if (fNScLanes==0) {
    std::ostringstream o;
    o << "fNScLanes = 0. That is forbitten!";
    G4Exception("MSDetectorConstruction::CrateScintillator","", FatalException,o.str().c_str());
  }
  
  if(fNScLanes % 2 != 0)
    positionY = -1*(fMSScintGap + fMSscintY)/2.*(fNScLanes/2);
  else
    positionY = -1*(fMSScintGap + fMSscintY)/2.*(fNScLanes/2+1);


  for(int iScLane = 0; iScLane<fNScLanes; iScLane++) {
    G4String nameTag = G4String((char)('0'+iScLane));

    G4String bigBoxName = bigScName+nameTag;
    G4VSolid* solidBigBar = new G4Box(bigBoxName, fMSscintX/2., fMSscintY/2., fMSscintZ/2.);
    G4LogicalVolume* logicBigBar = new G4LogicalVolume(solidBigBar, scintMaterial, bigBoxName); 
    // two middle lines filled by air!
    // if(iScLane==1 || iScLane==2) logicBigBar = new G4LogicalVolume(solidBigBar, scintMaterial_Air, bigBoxName);

    G4ThreeVector chPosition (0,positionY,fMSscintZshift);
    G4VPhysicalVolume* scintPhys =  new G4PVPlacement(0, chPosition, logicBigBar, bigBoxName, aParentLogicalVolume, false, 0);
    vPhysVolumesScint.push_back(scintPhys);

    positionY = positionY + (fMSScintGap + fMSscintY);
  }

  // return;

  if(fNScLanes % 2 != 0)
    positionY = -1*(fMSScintGap + fMSscintY)/2.*(fNScLanes/2-1);
  else
    positionY = -1*(fMSScintGap + fMSscintY)/2.*(fNScLanes/2);
  
  for(int iScLane = 0; iScLane<fNScLanes-1; iScLane++) {
    G4double positionX = fMSscintX/2.-fMSscintY/2.;
    if(iScLane % 2 == 0) // place samll scintalators on the left side first.
        positionX = -1*positionX;

    G4String nameTag = G4String((char)('0'+iScLane));
    G4String smallBoxName = smallScName+nameTag;
    G4VSolid* solidSmallBar = new G4Box(smallBoxName, fMSscintY/2., fMSScintGap/2., fMSscintZ/2.);
    G4LogicalVolume* logicSmallBar = new G4LogicalVolume(solidSmallBar, scintMaterial, smallBoxName); 
    G4VPhysicalVolume* scintPhys =  new G4PVPlacement(0, G4ThreeVector(positionX, positionY, fMSscintZshift), logicSmallBar, smallBoxName, aParentLogicalVolume, false, 0);
    vPhysVolumesScint.push_back(scintPhys);

    positionY = positionY + (fMSScintGap + fMSscintY);
  }
}

void MSDetectorConstruction::CreateScintSurface(G4VPhysicalVolume* borderhysicalVolume)
{
  G4OpticalSurface* opScintSurface = new G4OpticalSurface("ScintilatorSurface");
  opScintSurface->SetType(dielectric_dielectric);
  // opScintSurface->SetModel(unified);
  opScintSurface->SetModel(glisur);

  if (fSurfaceRoughness < 1.) {
    opScintSurface->SetFinish(ground);
    opScintSurface->SetPolish(fSurfaceRoughness);
  } else {
    opScintSurface->SetFinish(polished);
  }

  for(std::vector<G4VPhysicalVolume*>::iterator iScnt = vPhysVolumesScint.begin(); iScnt != vPhysVolumesScint.end(); ++iScnt) {
    new G4LogicalBorderSurface("ScintilatorSurfaceIn",*iScnt,borderhysicalVolume,opScintSurface);
    new G4LogicalBorderSurface("ScintilatorSurfaceOut",borderhysicalVolume,*iScnt,opScintSurface);
  }
}

void MSDetectorConstruction::CrateSiPM()
{
  // SiPM Surface Properties
  G4OpticalSurface* photonDetSurface = new G4OpticalSurface("PhotonDetSurface",glisur,polished,dielectric_metal,1);
  G4MaterialPropertiesTable* photonDetSurfaceProperty = new G4MaterialPropertiesTable();

  G4double lnm_sipm[] = { 301.621*nm, 330.610*nm, 338.751*nm, 349.481*nm, 357.529*nm, 
                          373.445*nm, 386.670*nm, 402.527*nm, 433.960*nm, 462.667*nm, 
                          483.489*nm, 509.462*nm, 532.779*nm, 561.248*nm, 581.933*nm, 
                          615.606*nm, 641.494*nm, 677.772*nm, 719.279*nm, 768.612*nm, 
                          812.776*nm, 862.143*nm, 901.138*nm, 940.133*nm, 968.738*nm };

  G4double effi_sipm[] = {  0.000000, 0.060724, 0.122832, 0.181678, 0.225812,
                            0.279762, 0.317375, 0.359891, 0.391001, 0.400000,
                            0.394370, 0.378080, 0.351987, 0.314463, 0.283460,
                            0.244313, 0.211686, 0.172544, 0.136682, 0.102469,
                            0.076415, 0.048738, 0.000000, 0.000000, 0.000000 };

  assert(sizeof(effi_sipm) == sizeof(lnm_sipm));
                            
  const G4int nbins = sizeof(lnm_sipm)/sizeof(G4double);
  G4double p_sipm[nbins];
  G4double refl_sipm[nbins];
  // G4cout << " QEFF OF SIPM: " << G4endl;
  for(int i = 0; i<nbins; i++)   {
    p_sipm[i] = CLHEP::twopi*CLHEP::hbarc/lnm_sipm[i];//eV;
    refl_sipm[i] = 0.0;
    // effi_sipm[i] = 0.99;
    // G4cout << p_sipm[i] << "[eV] :: " <<  lnm_sipm[i] << "[nm] :: eff = " << effi_sipm[i] << " :: refl = " << refl_sipm[i]<< G4endl;
  }

  assert(sizeof(refl_sipm) == sizeof(p_sipm));
  photonDetSurfaceProperty->AddProperty("REFLECTIVITY",p_sipm,refl_sipm,nbins);
  assert(sizeof(effi_sipm) == sizeof(p_sipm));
  photonDetSurfaceProperty->AddProperty("EFFICIENCY",p_sipm, effi_sipm, nbins);

  photonDetSurface->SetMaterialPropertiesTable(photonDetSurfaceProperty);

  // Creating volumes

  // looking for main channels
  for(std::vector<G4VPhysicalVolume*>::iterator iScnt = vPhysVolumesScint.begin(); iScnt != vPhysVolumesScint.end(); ++iScnt) {
    G4VPhysicalVolume* scintCh = *iScnt;
    G4String volumeName = scintCh->GetName();
    if(!volumeName.contains("MCh")) continue;

    G4int chNumber = GetChannelNumber(volumeName);
    G4String SiPMName = "SiPM_" + G4String((char)('0'+chNumber));

    G4VSolid* solidPhotonDet = new G4Box(SiPMName,fSiPMXsize/2.,fSiPMYsize/2.,fSiPMZsize/2.);
    G4LogicalVolume* logicPhotonDet = new G4LogicalVolume(solidPhotonDet, FindMaterial("G4_Al"),SiPMName);

    G4VisAttributes* visAttributesPhotonDet = new G4VisAttributes;
    visAttributesPhotonDet->SetColor(1.0, 0.0, 0.0);    // red
    logicPhotonDet->SetVisAttributes(visAttributesPhotonDet);

    // positioning
    fSiPMPositioning.setY(scintCh->GetTranslation().y());

    // construct
    G4VPhysicalVolume* SiPMPhys = new G4PVPlacement(0,fSiPMPositioning,logicPhotonDet,SiPMName,fLogVolumeWorld,false,0);
    new G4LogicalSkinSurface("PhotonDetSurface",logicPhotonDet,photonDetSurface);
    vPhysVolSiPM.push_back(SiPMPhys);
  }
}

void MSDetectorConstruction::CrateCollimator()
{
  G4String collimatorName = "Collimator";

  G4VSolid* solidCalimator = new G4Tubs(collimatorName, fCollimatorRIn, fCollimatorROut, fCollimatorZ/2., 0.*deg, 360.*deg);
  G4LogicalVolume* logicCalimator = new G4LogicalVolume(solidCalimator, FindMaterial("G4_Al"), collimatorName);
  G4VPhysicalVolume* collimatorPhys = new G4PVPlacement(0,fCollimatorPositioning,logicCalimator,collimatorName,fLogVolumeWorld,false,0);
  vPhysVolSupports.push_back(collimatorPhys);
}

void MSDetectorConstruction::ConstructSDandField()
{
  for(std::vector<G4VPhysicalVolume*>::iterator iScnt = vPhysVolSiPM.begin(); iScnt != vPhysVolSiPM.end(); ++iScnt) {
    G4VPhysicalVolume* sipmVolume = *iScnt;
    G4String detName = sipmVolume->GetName();

    G4String sipmSDName = "MS/PhotonDet/" + detName;

    MSPhotonDetSD* sipmSD = new MSPhotonDetSD(sipmSDName);
    sipmSD->ResetVectorPhysicalVolumes(GetVectorOfAllPhysicalVolumes());

    SetSensitiveDetector(detName, sipmSD);
  }
}

G4RotationMatrix MSDetectorConstruction::StringToRotationMatrix(G4String rotation)
{
  // We apply successive rotations OF THE OBJECT around the FIXED
  // axes of the parent's local coordinates; rotations are applied
  // left-to-right (rotation="r1,r2,r3" => r1 then r2 then r3).

  G4RotationMatrix rot;

  unsigned int place = 0;

  while (place < rotation.size()) {

        G4double angle;
        char* p;

        const G4String tmpstring=rotation.substr(place+1);

        angle = strtod(tmpstring.c_str(),&p) * deg;
 
        if (!p || (*p != (char)',' && *p != (char)'\0')) {
           G4cerr << "Invalid rotation specification: " <<
                                                  rotation.c_str() << G4endl;
           return rot;
        }

        G4RotationMatrix thisRotation;

        switch(rotation.substr(place,1).c_str()[0]) {
              case 'X': case 'x':
                thisRotation = G4RotationMatrix(CLHEP::HepRotationX(angle));
                break;
              case 'Y': case 'y':
                thisRotation = G4RotationMatrix(CLHEP::HepRotationY(angle));
                break;
              case 'Z': case 'z':
                thisRotation = G4RotationMatrix(CLHEP::HepRotationZ(angle));
                break;
              default:
                G4cerr << " Invalid rotation specification: "
                       << rotation << G4endl;
                return rot;
        }

       rot = thisRotation * rot;
       place = rotation.find(',',place);
       if (place > rotation.size()) break;
       ++place;
  }

  return rot;
}

G4Material* MSDetectorConstruction::FindMaterial(G4String name) 
{
    G4Material* material = G4Material::GetMaterial(name,true);
    return material;
}

G4int MSDetectorConstruction::GetChannelNumber(G4String aVolumeName)
{
  G4int chNumber = -1;

  if(aVolumeName(aVolumeName.first('M')+1)=='C' && aVolumeName(aVolumeName.first('M')+2)=='h' && aVolumeName(aVolumeName.first('M')+3)=='_') {
      G4String tmp = aVolumeName;
      tmp.remove(0,aVolumeName.first('M')+4);
      chNumber = std::stoi( tmp.data() );
  } else
  if(aVolumeName(aVolumeName.first('S')+1)=='C' && aVolumeName(aVolumeName.first('S')+2)=='h' && aVolumeName(aVolumeName.first('S')+3)=='_') {
      G4String tmp = aVolumeName;
      tmp.remove(0,aVolumeName.first('S')+4);
      chNumber = std::stoi( tmp.data() );
  } else {
    std::ostringstream o;
    o << "Impossible to recognize channel number.";
    G4Exception("MSDetectorConstruction::CrateScintillator","", FatalException,o.str().c_str());
  }
  // G4cout << "PhysVolumeName: " << aVolumeName << " and channel is " << chNumber << G4endl;
  return chNumber;
}

std::vector<G4VPhysicalVolume*> MSDetectorConstruction::GetVectorOfAllPhysicalVolumes()
{
  std::vector<G4VPhysicalVolume*> v = vPhysVolumesScint;
  v.insert(std::end(v), std::begin(vPhysVolSiPM), std::end(vPhysVolSiPM));
  v.insert(std::end(v), std::begin(vPhysVolSupports), std::end(vPhysVolSupports));
  return v;
}

std::vector<std::string> MSDetectorConstruction::GetVectorOfNamesAllPhysicalVolumes()
{
  std::vector<G4VPhysicalVolume*> v = GetVectorOfAllPhysicalVolumes();
  std::vector<std::string> vNames;
  for(auto i : v)
    vNames.push_back(i->GetName());

  return vNames;
}

std::vector<std::string> MSDetectorConstruction::GetVectorOfSiPMNames()
{
  std::vector<std::string> vNames;
  for(auto i : vPhysVolSiPM)
    vNames.push_back(i->GetName());

  return vNames;
}




