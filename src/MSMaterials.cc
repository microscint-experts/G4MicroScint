#include "MSMaterials.hh"

#include "G4SystemOfUnits.hh"

MSMaterials* MSMaterials::fInstance = 0;

MSMaterials::MSMaterials()
{
  fNistMan = G4NistManager::Instance();

  fNistMan->SetVerbose(2);

  CreateMaterials();
}

MSMaterials::~MSMaterials()
{
  delete    fHBP;
  delete    fEJ309;
  delete    fPMMA;
}

MSMaterials* MSMaterials::GetInstance()
{
  if (fInstance == 0)
  {
    fInstance = new MSMaterials();
  }
  return fInstance;
}

G4Material* MSMaterials::GetMaterial(const G4String material)
{
  G4Material* mat =  fNistMan->FindOrBuildMaterial(material);

  if (!mat) mat = G4Material::GetMaterial(material);
  if (!mat) {
     std::ostringstream o;
     o << "Material " << material << " not found!";
     G4Exception("MSMaterials::GetMaterial","",
                 FatalException,o.str().c_str());
  }

  return mat;
}

void MSMaterials::CreateMaterials()
{
  G4double density;
  // G4int ncomponents;
  // G4double fractionmass;
  std::vector<G4int> natoms;
  std::vector<G4double> fractionMass;
  std::vector<G4String> elements;

  // Materials Definitions
  // =====================

  //--------------------------------------------------
  // Vacuum
  //--------------------------------------------------

  fNistMan->FindOrBuildMaterial("G4_Galactic");

  //--------------------------------------------------
  // Air
  //--------------------------------------------------

  fAir = fNistMan->FindOrBuildMaterial("G4_AIR");

  //--------------------------------------------------
  // Aluminium
  //--------------------------------------------------

  fNistMan->FindOrBuildMaterial("G4_Al");

  //--------------------------------------------------
  // Plate PMMA
  //--------------------------------------------------

  elements.push_back("C");     natoms.push_back(5);
  elements.push_back("H");     natoms.push_back(8);
  elements.push_back("O");     natoms.push_back(2);

  density = 1.190*g/cm3;

  fPMMA = fNistMan->
          ConstructNewMaterial("PMMA", elements, natoms, density);

  elements.clear();
  natoms.clear();

  //--------------------------------------------------
  // Plate HBP
  //--------------------------------------------------

  // TODO: Fix nAtoms number!
  elements.push_back("C");     natoms.push_back(43);
  elements.push_back("H");     natoms.push_back(52);
  elements.push_back("O");     natoms.push_back(18);

  // TODO: Fix density!
  density = 1.20*g/cm3;

  fHBP = fNistMan->ConstructNewMaterial("HBP", elements, natoms, density);

  elements.clear();
  natoms.clear();

  //--------------------------------------------------
  //  Scintillator EJ-309
  //--------------------------------------------------

  // TODO: Fix nAtoms number!
  elements.push_back("C");     natoms.push_back(6);
  elements.push_back("H");     natoms.push_back(6);

  density = 0.964*g/cm3; // http://www.johncaunt.com/detectors/scintillation-detectors/2307-2/

  fEJ309 = fNistMan->ConstructNewMaterial("EJ309", elements, natoms, density);

  elements.clear();
  natoms.clear();


  //
  // ------------ Generate & Add Material Properties Table ------------
  //

  G4double photonEnergy[] =
  {2.00*eV,2.03*eV,2.06*eV,2.09*eV,2.12*eV,
   2.15*eV,2.18*eV,2.21*eV,2.24*eV,2.27*eV,
   2.30*eV,2.33*eV,2.36*eV,2.39*eV,2.42*eV,
   2.45*eV,2.48*eV,2.51*eV,2.54*eV,2.57*eV,
   2.60*eV,2.63*eV,2.66*eV,2.69*eV,2.72*eV,
   2.75*eV,2.78*eV,2.81*eV,2.84*eV,2.87*eV,
   2.90*eV,2.93*eV,2.96*eV,2.99*eV,3.02*eV,
   3.05*eV,3.08*eV,3.11*eV,3.14*eV,3.17*eV,
   3.20*eV,3.23*eV,3.26*eV,3.29*eV,3.32*eV,
   3.35*eV,3.38*eV,3.41*eV,3.44*eV,3.47*eV};

  const G4int nEntries = sizeof(photonEnergy)/sizeof(G4double);

  //--------------------------------------------------
  // Air
  //--------------------------------------------------

  G4double refractiveIndex[] =
  { 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00,
    1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00,
    1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00,
    1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00,
    1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00};

  assert(sizeof(refractiveIndex) == sizeof(photonEnergy));

  G4MaterialPropertiesTable* mpt = new G4MaterialPropertiesTable();
  mpt->AddProperty("RINDEX", photonEnergy, refractiveIndex, nEntries);

  fAir->SetMaterialPropertiesTable(mpt);

  //--------------------------------------------------
  //  PMMA for WLSfibers
  //--------------------------------------------------

  G4double refractiveIndexWLSfiber[] =
  { 1.60, 1.60, 1.60, 1.60, 1.60, 1.60, 1.60, 1.60, 1.60, 1.60,
    1.60, 1.60, 1.60, 1.60, 1.60, 1.60, 1.60, 1.60, 1.60, 1.60,
    1.60, 1.60, 1.60, 1.60, 1.60, 1.60, 1.60, 1.60, 1.60, 1.60,
    1.60, 1.60, 1.60, 1.60, 1.60, 1.60, 1.60, 1.60, 1.60, 1.60,
    1.60, 1.60, 1.60, 1.60, 1.60, 1.60, 1.60, 1.60, 1.60, 1.60};

  assert(sizeof(refractiveIndexWLSfiber) == sizeof(photonEnergy));

  G4double absWLSfiber[] =
  {5.40*m,5.40*m,5.40*m,5.40*m,5.40*m,5.40*m,5.40*m,5.40*m,5.40*m,5.40*m,
   5.40*m,5.40*m,5.40*m,5.40*m,5.40*m,5.40*m,5.40*m,5.40*m,5.40*m,5.40*m,
   5.40*m,5.40*m,5.40*m,5.40*m,5.40*m,5.40*m,5.40*m,5.40*m,5.40*m,1.10*m,
   1.10*m,1.10*m,1.10*m,1.10*m,1.10*m,1.10*m, 1.*mm, 1.*mm, 1.*mm, 1.*mm,
    1.*mm, 1.*mm, 1.*mm, 1.*mm, 1.*mm, 1.*mm, 1.*mm, 1.*mm, 1.*mm, 1.*mm};

  assert(sizeof(absWLSfiber) == sizeof(photonEnergy));

  G4double emissionFib[] =
  {0.05, 0.10, 0.30, 0.50, 0.75, 1.00, 1.50, 1.85, 2.30, 2.75,
   3.25, 3.80, 4.50, 5.20, 6.00, 7.00, 8.50, 9.50, 11.1, 12.4,
   12.9, 13.0, 12.8, 12.3, 11.1, 11.0, 12.0, 11.0, 17.0, 16.9,
   15.0, 9.00, 2.50, 1.00, 0.05, 0.00, 0.00, 0.00, 0.00, 0.00,
   0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00};

  assert(sizeof(emissionFib) == sizeof(photonEnergy));

  // Add entries into properties table
  G4MaterialPropertiesTable* mptWLSfiber = new G4MaterialPropertiesTable();
  mptWLSfiber->AddProperty("RINDEX",photonEnergy,refractiveIndexWLSfiber,nEntries);
  mptWLSfiber->AddProperty("ABSLENGTH",photonEnergy,absWLSfiber,nEntries);
  mptWLSfiber->AddProperty("WLSCOMPONENT",photonEnergy,emissionFib,nEntries);
  mptWLSfiber->AddConstProperty("WLSTIMECONSTANT", 0.5*ns);

  fPMMA->SetMaterialPropertiesTable(mptWLSfiber);

  //--------------------------------------------------
  //  HBP for plate
  //--------------------------------------------------

  G4double refractiveIndexHBP[] =
  { 1.48, 1.48, 1.48, 1.48, 1.48, 1.48, 1.48, 1.48, 1.48, 1.48,
    1.48, 1.48, 1.48, 1.48, 1.48, 1.48, 1.48, 1.48, 1.48, 1.48,
    1.48, 1.48, 1.48, 1.48, 1.48, 1.48, 1.48, 1.48, 1.48, 1.48,
    1.48, 1.48, 1.48, 1.48, 1.48, 1.48, 1.48, 1.48, 1.48, 1.48,
    1.48, 1.48, 1.48, 1.48, 1.48, 1.48, 1.48, 1.48, 1.48, 1.48};

  assert(sizeof(refractiveIndexHBP) == sizeof(photonEnergy));

  G4double photonEnergy_HBP_ABS[] =
  { 1.54*eV,1.56*eV,1.59*eV,1.65*eV,1.71*eV,
    1.80*eV,1.85*eV,1.93*eV,2.02*eV,2.10*eV,
    2.23*eV,2.33*eV,2.44*eV,2.67*eV,2.80*eV,
    2.87*eV,2.98*eV,2.98*eV,3.00*eV,3.02*eV,
    3.04*eV,3.05*eV,3.06*eV,3.11*eV,3.13*eV,
    3.17*eV,3.24*eV,3.40*eV,3.64*eV,3.84*eV,
    3.96*eV,3.99*eV
  };

  const G4int nEntries_HBP_ABS = sizeof(photonEnergy_HBP_ABS)/sizeof(G4double);

  G4double absHBP[] =
  { 10.46*mm,10.39*mm,10.15*mm, 9.70*mm, 9.32*mm,
     8.79*mm, 8.54*mm, 8.14*mm, 7.75*mm, 7.39*mm,
     6.92*mm, 6.57*mm, 6.24*mm, 5.63*mm, 5.30*mm,
     5.15*mm, 4.92*mm, 4.86*mm, 4.77*mm, 4.64*mm,
     4.53*mm, 4.43*mm, 4.31*mm, 4.10*mm, 3.93*mm,
     3.73*mm, 3.43*mm, 3.02*mm, 2.55*mm, 2.33*mm,
     2.26*mm,0.85*mm
  };

  assert(sizeof(absHBP) == sizeof(photonEnergy_HBP_ABS));

  // TODO: Are you sure? Blame Nastia if not :)
  G4double emissionHBP[] =
  {0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00,
   0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00,
   0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00,
   0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00,
   0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00};

  assert(sizeof(emissionHBP) == sizeof(photonEnergy));

  // Add entries into properties table
  G4MaterialPropertiesTable* mptHBP = new G4MaterialPropertiesTable();
  mptHBP->AddProperty("RINDEX",photonEnergy,refractiveIndexHBP,nEntries);
  mptHBP->AddProperty("ABSLENGTH",photonEnergy,absHBP,nEntries_HBP_ABS);
  mptHBP->AddProperty("WLSCOMPONENT",photonEnergy,emissionHBP,nEntries);

  fHBP->SetMaterialPropertiesTable(mptHBP);

  //--------------------------------------------------
  //  EJ-309 for scintillator
  //--------------------------------------------------
  G4double refractiveIndexPS[] =
  { 1.57, 1.57, 1.57, 1.57, 1.57, 1.57, 1.57, 1.57, 1.57, 1.57,
    1.57, 1.57, 1.57, 1.57, 1.57, 1.57, 1.57, 1.57, 1.57, 1.57,
    1.57, 1.57, 1.57, 1.57, 1.57, 1.57, 1.57, 1.57, 1.57, 1.57,
    1.57, 1.57, 1.57, 1.57, 1.57, 1.57, 1.57, 1.57, 1.57, 1.57,
    1.57, 1.57, 1.57, 1.57, 1.57, 1.57, 1.57, 1.57, 1.57, 1.57};

  assert(sizeof(refractiveIndexPS) == sizeof(photonEnergy));

  G4double absPS[] =
  {210.*cm,210.*cm,210.*cm,210.*cm,210.*cm,210.*cm,210.*cm,210.*cm,210.*cm,210.*cm,
   210.*cm,210.*cm,210.*cm,210.*cm,210.*cm,210.*cm,210.*cm,210.*cm,210.*cm,210.*cm,
   210.*cm,210.*cm,210.*cm,210.*cm,210.*cm,210.*cm,210.*cm,210.*cm,210.*cm,210.*cm,
   210.*cm,210.*cm,210.*cm,210.*cm,210.*cm,210.*cm,210.*cm,210.*cm,210.*cm,210.*cm,
   210.*cm,210.*cm,210.*cm,210.*cm,210.*cm,210.*cm,210.*cm,210.*cm,210.*cm,210.*cm};

  assert(sizeof(absPS) == sizeof(photonEnergy));

  G4double scintilFast[] = 
  {0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 
   0.00, 0.00, 0.06, 0.06, 0.10, 0.13, 0.16, 0.19, 0.25, 0.31, 
   0.35, 0.41, 0.46, 0.50, 0.54, 0.64, 0.72, 0.75, 0.77, 0.85, 
   1.00, 0.99, 0.97, 0.84, 0.81, 0.70, 0.68, 0.65, 0.50, 0.30, 
   0.22, 0.05, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00
  };

  assert(sizeof(scintilFast) == sizeof(photonEnergy));

  // Add entries into properties table
  G4MaterialPropertiesTable* mptEJ309 = new G4MaterialPropertiesTable();
  mptEJ309->AddProperty("RINDEX",photonEnergy,refractiveIndexPS,nEntries);
  mptEJ309->AddProperty("ABSLENGTH",photonEnergy,absPS,nEntries);
  mptEJ309->AddProperty("FASTCOMPONENT",photonEnergy, scintilFast,nEntries);
  mptEJ309->AddConstProperty("SCINTILLATIONYIELD",12000./MeV);
  mptEJ309->AddConstProperty("RESOLUTIONSCALE",1.0);
  mptEJ309->AddConstProperty("FASTTIMECONSTANT", 3.5*ns);

  fEJ309->SetMaterialPropertiesTable(mptEJ309);

  // Set the Birks Constant for the Polystyrene scintillator

  fEJ309->GetIonisation()->SetBirksConstant(0.01*mm/MeV);

}
