#include "globals.hh"
#include "G4RunManager.hh"

#include "MSUserTrackInformation.hh"

#include "G4Track.hh"
#include "G4TrackingManager.hh"

#include "MSTrackingAction.hh"

void MSTrackingAction::PreUserTrackingAction(const G4Track* aTrack)
{
  MSUserTrackInformation* trackInformation = new MSUserTrackInformation();
  
  trackInformation->SetOriginalPhysicalVolume(aTrack->GetVolume());

  fpTrackingManager->SetUserTrackInformation(trackInformation);
}

void MSTrackingAction::PostUserTrackingAction(const G4Track*)
{
}
