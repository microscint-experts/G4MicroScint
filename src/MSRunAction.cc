#include "G4Timer.hh"
#include "G4Run.hh"

#include "MSRunAction.hh"

#include "HistController.hh"
#include "HistogramStore.hh"
static HistogramStore& histStore = *HistogramStore::getInstance();


MSRunAction::MSRunAction()
  : G4UserRunAction(), fTimer(0)
{
  fTimer = new G4Timer;
}

MSRunAction::~MSRunAction()
{
  delete fTimer;
}

void MSRunAction::BeginOfRunAction(const G4Run*)
{
  fTimer->Start();
  histStore.fillTH1F("hStatistics",HistController::CutFlow::TOTAL_RUN);
}

void MSRunAction::EndOfRunAction(const G4Run* aRun)
{
  fTimer->Stop();
  G4cout << "Stat:: number of event = " << aRun->GetNumberOfEvent() << " " << *fTimer << G4endl;
}
