#include "HistController.hh"

#include "HistogramStore.hh"
static HistogramStore& histStore = *HistogramStore::getInstance();

#include "MSDetectorConstruction.hh"

#include <string>
#include <iostream>

using namespace std;

HistController::HistController()
{	
	fDetector = 0x0;
}

HistController::~HistController()
{
	std::vector<TH1*> hists = histStore.getListOfHistograms();
	for(auto h : hists)
		delete(h);
}

void HistController::Initialize(MSDetectorConstruction* detConstruction)
{
	fDetector = detConstruction;

	cout<<"Creating histograms...." << endl;
	histInitialize_Statistics();
	histInitialize_EventInfo();
	histInitialize_SiPMSD();
}

void HistController::histInitialize_Statistics()
{
	const std::vector<std::string> xlabels_hStatistics = {"Total Runs","Total Events", "SiPM"};
	TH1F* hTempPointer = 0x0;
	histStore.createTH1F("hStatistics",  NCUT, 0, NCUT, "Number of Events;<#mu>;Events");
	hTempPointer = histStore.getTH1F("hStatistics");
	for (unsigned i = 0; i != NCUT; ++i) { hTempPointer->GetXaxis()->SetBinLabel(i + 1, xlabels_hStatistics[i].c_str()); }
}

void HistController::histInitialize_EventInfo()
{
	histStore.createTH1F("hEventInfo_PhotProcess_Unknown", 			5000, 0, 5000, "Total unknown photons;# photons; events");
	histStore.createTH1F("hEventInfo_PhotProcess_Total", 			5000, 0, 5000, "Total photons;# photons; events");
	histStore.createTH1F("hEventInfo_PhotProcess_Scintillation", 	5000, 0, 5000, "Total scintillation photons;# photons; events");
	histStore.createTH1F("hEventInfo_PhotProcess_Cerenkov", 		5000, 0, 5000, "Total cerenkov photons;# photons; events");

	histStore.createTH1F("hEventInfo_Energy_PhotProcess_Unknown", 			750, 0, 750, "Total #gamma (unknown process);# #lambda_{#gamma} [nm]; frequency");
	histStore.createTH1F("hEventInfo_Energy_PhotProcess_Total", 			750, 0, 750, "Total #gamma;#lambda_{#gamma} [nm]; frequency");
	histStore.createTH1F("hEventInfo_Energy_PhotProcess_Scintillation", 	750, 0, 750, "Total scintillation photons;#lambda_{#gamma} [nm]; frequency");
	histStore.createTH1F("hEventInfo_Energy_PhotProcess_Cerenkov", 			750, 0, 750, "Total cerenkov photons;#lambda_{#gamma} [nm]; frequency");

	histStore.createTH3F("hEventInfo_PrimaryParticlesOnPlateSurface", 200, -10, 10, 200, -10, 10, 1250, 0, 2500,  "Primary particles on plate surface; X [mm]; Y [mm]; E_{primary} [keV]");
}


void HistController::histInitialize_SiPMSD()
{
	if(!fDetector) {
		std::ostringstream o;
	    o << "Empty pointer fDetector. Make sure you did SetDetector() for propper detector!";
	    G4Exception("HistController::histInitialize_SiPMSD","", FatalException,o.str().c_str());
	}

	std::vector<std::string> vSiPMNames = fDetector->GetVectorOfSiPMNames();
	std::vector<std::string> vAllVolumesNames = fDetector->GetVectorOfNamesAllPhysicalVolumes();

	for(auto sSiPM : vSiPMNames) {
		TString sipm = sSiPM;
		// Process
		histStore.createTH1F(Form("h%s_PhotProcess_Unknown",sipm.Data()), 		5000, 0, 5000, Form("%s (unknown process);# detected photons; frequency",		sipm.Data()));
		histStore.createTH1F(Form("h%s_PhotProcess_Total",sipm.Data()), 		5000, 0, 5000, Form("%s total registered;# detected photons; frequency", 					sipm.Data()));
		histStore.createTH1F(Form("h%s_PhotProcess_Cerenkov",sipm.Data()), 		5000, 0, 5000, Form("%s (Cerenkov process);# detected photons; frequency",		sipm.Data()));
		histStore.createTH1F(Form("h%s_PhotProcess_Scintillation",sipm.Data()), 5000, 0, 5000, Form("%s (Scintillation process);# detected photons; frequency",	sipm.Data()));

		histStore.createTH1F(Form("h%s_Energy_PhotProcess_Unknown",sipm.Data()), 		750, 0, 750, Form("%s (unknown process);# #lambda_{#gamma} [nm]; frequency",		sipm.Data()));
		histStore.createTH1F(Form("h%s_Energy_PhotProcess_Total",sipm.Data()), 			750, 0, 750, Form("%s total registered;# #lambda_{#gamma} [nm]; frequency",					sipm.Data()));
		histStore.createTH1F(Form("h%s_Energy_PhotProcess_Cerenkov",sipm.Data()), 		750, 0, 750, Form("%s (Cerenkov process);# #lambda_{#gamma} [nm]; frequency",		sipm.Data()));
		histStore.createTH1F(Form("h%s_Energy_PhotProcess_Scintillation",sipm.Data()), 	750, 0, 750, Form("%s (Scintillation process);# #lambda_{#gamma} [nm]; frequency",	sipm.Data()));

		// Location
		for(auto sVolume : vAllVolumesNames) {
			TString volume = sVolume;
			histStore.createTH1F(Form("h%s_PhotFrom_%s",sipm.Data(), volume.Data()), 	5000, 0, 5000, Form("%s (born in %s);# detected photons; frequency",sipm.Data(),volume.Data()));
		}
	}
}


void HistController::DumpHistsToRootFile(std::string fileName)
{
	TFile *OutFile = new TFile(fileName.c_str(),"RECREATE");
	if (OutFile->IsOpen()) printf("Root File \"%s\" for to store all histograms opened successfully\n", fileName.c_str());

	std::vector<TH1*> hists = histStore.getListOfHistograms();
	cout << "Hists to be dumped: " <<hists.size() << endl;
	for(auto hist : hists) {
		G4cout << hist->GetName() << G4endl;
		if(!OutFile->FindObjectAny(hist->GetName()))
			hist->Write();
		else // In general it is impossible, but who knows users, right? :)
			std::cout << "ERROR\n Hist " << hist->GetName() << " already exists in the \"" << fileName << "\" root file! Skip it.\n";
	}
	OutFile->Close();
	cout << "Done! " << endl;
}