#include "MSPrimaryGeneratorAction.hh"
#include "MSDetectorConstruction.hh"

#include "G4ParticleGun.hh"
#include "G4SingleParticleSource.hh"
#include "G4SPSAngDistribution.hh"
#include "G4Electron.hh"
#include "G4SystemOfUnits.hh"
#include "G4ParticleTable.hh"
#include "Randomize.hh"

#include <math.h>

MSPrimaryGeneratorAction::MSPrimaryGeneratorAction(MSDetectorConstruction* det) 
: G4VUserPrimaryGeneratorAction(), fDetector(det)
{
    //opening angle
    //
    G4double alphaMin =  0.*deg;
    G4double alphaMax = 1.*deg;
    fCosAlphaMin = std::cos(alphaMin);
    fCosAlphaMax = std::cos(alphaMax); 

    // particle gun
    fParticleGun = new G4ParticleGun();

    ParticleGun();
}

MSPrimaryGeneratorAction::~MSPrimaryGeneratorAction()
{
    if(fParticleGun) 
        delete fParticleGun;
}

void MSPrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{
    // FixedGunDirection();
    RandomGunDirectoin();
    
    fParticleGun->GeneratePrimaryVertex(anEvent);
}

void MSPrimaryGeneratorAction::ParticleGun()
{
    G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
    G4ParticleDefinition* particle = particleTable->FindParticle("e-");
    fParticleGun->SetParticleDefinition(particle);
    fParticleGun->SetParticleTime(0.0*ns);

    G4ThreeVector gunPositioning = fDetector->GetCollimatorPositioning();
    gunPositioning.setZ(gunPositioning.z()+1*cm);
    fParticleGun->SetParticlePosition(G4ThreeVector(gunPositioning));
    fParticleGun->SetParticleEnergy(2.23*MeV); // 2.23*MeV
}

void MSPrimaryGeneratorAction::RandomGunDirectoin()
{
    //particle direction uniform around ur 
    //    
    //1- in World frame
    //cosAlpha uniform in [cos(alphaMin), cos(alphaMax)]
    G4double cosAlpha = fCosAlphaMin-G4UniformRand()*(fCosAlphaMin-fCosAlphaMax);
    G4double sinAlpha = std::sqrt(1. - cosAlpha*cosAlpha);
    G4double psi      = CLHEP::twopi*G4UniformRand();  //psi uniform in (0,2*pi)  
    G4ThreeVector dir(sinAlpha*std::cos(psi),sinAlpha*std::sin(psi),cosAlpha);
    

    dir.setTheta(dir.getTheta() + 180*deg);

    // G4cout << "Emission Direction: r: (" << dir.getTheta()/deg << ", " << dir.getPhi()/deg << ")" << G4endl;

    fParticleGun->SetParticleMomentumDirection(dir);
}

void MSPrimaryGeneratorAction::FixedGunDirection()
{   
    fParticleGun->SetParticlePosition(G4ThreeVector(-3.5*cm,0.5*mm,0.3*cm));
    fParticleGun->SetParticleMomentumDirection(G4ThreeVector(0.,0.,-1.));
}
