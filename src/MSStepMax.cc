#include "MSStepMax.hh"

#include "G4Track.hh"
#include "G4VParticleChange.hh"

MSStepMax::MSStepMax(const G4String& aName)
  : G4VDiscreteProcess(aName), fMaxChargedStep(DBL_MAX)
{
   if (verboseLevel>0) {
     G4cout << GetProcessName() << " is created "<< G4endl;
   }
}

MSStepMax::~MSStepMax() { }

MSStepMax::MSStepMax(MSStepMax& right) : G4VDiscreteProcess(right) { }

G4bool MSStepMax::IsApplicable(const G4ParticleDefinition& particle)
{
  return (particle.GetPDGCharge() != 0.);
}

void MSStepMax::SetStepMax(G4double step) { fMaxChargedStep = step ; }

G4double MSStepMax::PostStepGetPhysicalInteractionLength( const G4Track&, G4double, G4ForceCondition* condition)
{
  // condition is set to "Not Forced"
  *condition = NotForced;

  G4double ProposedStep = DBL_MAX;

  if ( fMaxChargedStep > 0.) ProposedStep = fMaxChargedStep;

   return ProposedStep;
}

G4VParticleChange* MSStepMax::PostStepDoIt(const G4Track& aTrack,
                                         const G4Step&         )
{
   // do nothing
   aParticleChange.Initialize(aTrack);
   return &aParticleChange;
}

G4double MSStepMax::GetMeanFreePath(const G4Track&,G4double,G4ForceCondition*)
{
  return 0.;
}
