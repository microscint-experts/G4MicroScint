#include "MSPhotonDetSD.hh"
#include "MSUserTrackInformation.hh"
// #include "MSDetectorConstruction.hh" // TODO: We must get names from MSDetectorConstruction and count in in auto mode.

#include "HistController.hh"
#include "HistogramStore.hh"
static HistogramStore& histStore = *HistogramStore::getInstance();

#include "G4Track.hh"
#include "G4ThreeVector.hh"
#include "G4Step.hh"
#include "G4ParticleDefinition.hh"
#include "G4VTouchable.hh"
#include "G4TouchableHistory.hh"
#include "G4ios.hh"
#include "G4ParticleTypes.hh"
#include "G4ParticleDefinition.hh"
#include "G4VProcess.hh"
#include "G4VUserTrackInformation.hh"
#include "G4SystemOfUnits.hh"

#include "TString.h"
#include <string>
#include <iostream>

MSPhotonDetSD::MSPhotonDetSD(G4String name) : G4VSensitiveDetector(name)
{
  ResetVars();

  vSiPMPhysVolumes.clear();
  vDetPhysVolumes.clear();
}

MSPhotonDetSD::~MSPhotonDetSD() {}

void MSPhotonDetSD::Initialize(G4HCofThisEvent* )
{
  ResetVars();
}

void MSPhotonDetSD::ResetVars()
{
  fTotalCounter                 = 0;
  fUnknownProcessCounter        = 0;
  fScintillationProcessCounter  = 0;
  fCerenkovProcessCounter       = 0;

  ResetVectorPhysicalVolumes(vDetPhysVolumes);
}

G4bool MSPhotonDetSD::ProcessHits(G4Step* aStep, G4TouchableHistory* )
{
  if (aStep == NULL) return true;
  G4Track* theTrack = aStep->GetTrack();

  // Need to know if this is an optical photon
  if(theTrack->GetDefinition() != G4OpticalPhoton::OpticalPhotonDefinition()) return true;

  // Do we need to select only optical photon here? Other particles willl be storeged in the fUnknownProcessCounter variable.
  // G4String ParticleName = theTrack->GetDynamicParticle()->GetParticleDefinition()->GetParticleName();
  // if (ParticleName == "opticalphoton") return;

  PhotonCounter_Total(theTrack);
  PhotonCounter_Processes(theTrack);
  PhotonCounter_Location(theTrack);


  // SiPM is a small black hole :)
  theTrack->SetTrackStatus(fStopAndKill);
  return true;
}

void MSPhotonDetSD::PhotonCounter_Total(G4Track* aTrack)
{
  fTotalCounter++;

  G4double energy = aTrack->GetDynamicParticle()->GetTotalEnergy();
  G4double wavelength = CLHEP::twopi*CLHEP::hbarc/energy/eV;
  histStore.fillTH1F(Form("h%s_Energy_PhotProcess_Total",this->GetName().data()), wavelength);
}

void MSPhotonDetSD::PhotonCounter_Processes(G4Track* aTrack)
{
  G4String creatorProcessName = aTrack->GetCreatorProcess()->GetProcessName();

  G4double energy = aTrack->GetDynamicParticle()->GetTotalEnergy();
  G4double wavelength = CLHEP::twopi*CLHEP::hbarc/energy/eV;

  if (creatorProcessName == "Scintillation") {
    fScintillationProcessCounter++;
    histStore.fillTH1F(Form("h%s_Energy_PhotProcess_Scintillation",this->GetName().data()), wavelength);
  }
  else
  if (creatorProcessName == "Cerenkov") {
    fCerenkovProcessCounter++;
    histStore.fillTH1F(Form("h%s_Energy_PhotProcess_Cerenkov",this->GetName().data()), wavelength);
  }
  else {
    G4cout << " Unknown for MSSteppingAction::UserSteppingAction Creator Process : " << creatorProcessName << G4endl;
    fUnknownProcessCounter++;
    histStore.fillTH1F(Form("h%s_Energy_PhotProcess_Unknown",this->GetName().data()), wavelength);
  }
}

void MSPhotonDetSD::PhotonCounter_Location(G4Track* aTrack)
{
  MSUserTrackInformation* trackInformation = (MSUserTrackInformation*)aTrack->GetUserInformation();
  G4String origVolumeName = trackInformation->GetOriginalPhysicalVolume()->GetName();

  // Location counter
  if(fMapPhotFromCounters.count(origVolumeName)>0)
    fMapPhotFromCounters[origVolumeName]++;
  else {
    std::ostringstream o;
    o << "There are no entry for volume name \"" << origVolumeName << "\" in the fMapPhotFromCounters.";
    G4Exception("MSPhotonDetSD::PhotonCounter_Location","", FatalException, o.str().c_str());
  }

  // Lets fill photons energy per location
}

void MSPhotonDetSD::EndOfEvent(G4HCofThisEvent*)
{
  // cutflow
  if(fTotalCounter) histStore.fillTH1F("hStatistics",HistController::CutFlow::SiPM);

   // Process
  G4cout<< "MSPhotonDetSD::EndOfEvent() There are " << fTotalCounter << " optical photons registered in the " << this->GetName() << "!" << G4endl;
  histStore.fillTH1F(Form("h%s_PhotProcess_Total",this->GetName().data()),fTotalCounter);
  histStore.fillTH1F(Form("h%s_PhotProcess_Unknown",      this->GetName().data()), fUnknownProcessCounter       );
  histStore.fillTH1F(Form("h%s_PhotProcess_Cerenkov",     this->GetName().data()), fCerenkovProcessCounter      );
  histStore.fillTH1F(Form("h%s_PhotProcess_Scintillation",this->GetName().data()), fScintillationProcessCounter );

  // Where is photons from?
  for(auto iLocation : fMapPhotFromCounters) {
    TString locationName = iLocation.first;
    G4int counter = iLocation.second;
    histStore.fillTH1F(Form("h%s_PhotFrom_%s",this->GetName().data(),locationName.Data()), counter );
  }

  ResetVars();
}

void MSPhotonDetSD::ResetVectorPhysicalVolumes(std::vector<G4VPhysicalVolume*> vL) 
{ 
  vDetPhysVolumes = vL; 
  for(auto iVolume : vDetPhysVolumes) {
    TString volumeName = iVolume->GetName();
    // if(fMapPhotFromCounters.count(volumeName)>0){
    //   G4cout  << "In the SD " << this->GetName() << " volume " << volumeName 
    //           << " has counts = " << fMapPhotFromCounters[volumeName] << "! Resetting it." << G4endl;
    // }

    // Setting all 0, or creating it
    fMapPhotFromCounters[volumeName] = 0;
  }
}
