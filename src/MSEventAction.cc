#include "G4RunManager.hh"
#include "G4Run.hh"

#include "MSEventAction.hh"

#include "HistController.hh"
#include "HistogramStore.hh"
static HistogramStore& histStore = *HistogramStore::getInstance();


MSEventAction::MSEventAction():G4UserEventAction()
{
}

MSEventAction::~MSEventAction()
{
}

void MSEventAction::BeginOfEventAction(const G4Event* aEvent)
{
	G4int runNumber   = G4RunManager::GetRunManager()->GetCurrentRun()->GetRunID();
	G4int eventNumber = aEvent->GetEventID();
  G4cout << "\n Run №" << runNumber << " # Event No.: " << eventNumber << G4endl;
}

void MSEventAction::EndOfEventAction(const G4Event*)
{
 	 histStore.fillTH1F("hStatistics",HistController::CutFlow::TOTAL_EV);
}