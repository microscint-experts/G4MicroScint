#include "MSActionInitialization.hh"
#include "MSDetectorConstruction.hh"

#include "MSPrimaryGeneratorAction.hh"
#include "MSRunAction.hh"
#include "MSEventAction.hh"
#include "MSTrackingAction.hh"
#include "MSSteppingAction.hh"

MSActionInitialization::MSActionInitialization(MSDetectorConstruction* det)
 : G4VUserActionInitialization(), fDetector(det)
{
}

MSActionInitialization::~MSActionInitialization()
{
}

void MSActionInitialization::BuildForMaster() const
{
  SetUserAction(new MSRunAction());
}

void MSActionInitialization::Build() const
{
  SetUserAction(new MSPrimaryGeneratorAction(fDetector));
  SetUserAction(new MSRunAction());
  SetUserAction(new MSEventAction());
  SetUserAction(new MSTrackingAction());
  SetUserAction(new MSSteppingAction());
}
