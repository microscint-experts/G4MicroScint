#include "MSSteppingAction.hh"
#include "MSUserTrackInformation.hh"

#include "HistController.hh"
#include "HistogramStore.hh"
static HistogramStore& histStore = *HistogramStore::getInstance();

#include "G4Step.hh"
#include "G4StepPoint.hh"
#include "G4Track.hh"
#include "G4OpticalPhoton.hh"

#include "G4Event.hh"
#include "G4RunManager.hh"
#include "G4VUserTrackInformation.hh"
#include "G4VPhysicalVolume.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4SystemOfUnits.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4TouchableHandle.hh"

#include "G4String.hh"
#include "G4Box.hh"


MSSteppingAction::MSSteppingAction() : G4UserSteppingAction()
{ 
  fEventNumber          = -1;
  ResetVars();
}

MSSteppingAction::~MSSteppingAction() { }


void MSSteppingAction::ResetVars()
{
  fTotalCounter         = 0;
  fUnknownCounter       = 0;
  fScintillationCounter = 0;
  fCerenkovCounter      = 0;
}


void MSSteppingAction::UserSteppingAction(const G4Step* step)
{
  G4int eventNumber = G4RunManager::GetRunManager()->GetCurrentEvent()->GetEventID();

  if (eventNumber != fEventNumber) { // detect new run
    /*
    G4cout << " Number of Total born Photons in previous event: "           << fTotalCounter          << G4endl;
    G4cout << " Number of Photons from unknown process in previous event: " << fUnknownCounter        << G4endl;
    G4cout << " Number of Cerenkov Photons in previous event: "             << fCerenkovCounter       << G4endl;
    G4cout << " Number of Scintillation Photons in previous event: "        << fScintillationCounter  << G4endl;
    G4cout << " Number of Cerenkov Photons in previous event: "             << fCerenkovCounter       << G4endl;
    //*/
    
    FillHists();

    fEventNumber = eventNumber;
    ResetVars();
  }

  TakeActionsForBornOpticalPhoton(step);

  TakeActionsForPrimaryParticlesOnPlateSurface(step);
}

void MSSteppingAction::FillHists()
{
  histStore.fillTH1F("hEventInfo_PhotProcess_Total",         fTotalCounter        );
  histStore.fillTH1F("hEventInfo_PhotProcess_Unknown",       fUnknownCounter      );
  histStore.fillTH1F("hEventInfo_PhotProcess_Cerenkov",      fCerenkovCounter     );
  histStore.fillTH1F("hEventInfo_PhotProcess_Scintillation", fScintillationCounter);
}

void MSSteppingAction::TakeActionsForBornOpticalPhoton(const G4Step* step)
{
  // Idea here is to select original particles (e-/e+ and other) that can produce optical photon 
  // and using GetSecondaryInCurrentStep() count all secondary optical photons.
  G4Track* track = step->GetTrack();
  G4String ParticleName = track->GetDynamicParticle()->GetParticleDefinition()->GetParticleName();
  if (ParticleName == "opticalphoton") return;

  const std::vector<const G4Track*>* secondaries = step->GetSecondaryInCurrentStep();

  if (secondaries->size()>0) {
    for(unsigned int i=0; i<secondaries->size(); ++i) {
      if (secondaries->at(i)->GetParentID()>0) {
        if(secondaries->at(i)->GetDynamicParticle()->GetParticleDefinition() == G4OpticalPhoton::OpticalPhotonDefinition()) {
          G4String processName = secondaries->at(i)->GetCreatorProcess()->GetProcessName();
          G4double energy = secondaries->at(i)->GetTotalEnergy();
          G4double wavelength = CLHEP::twopi*CLHEP::hbarc/energy/eV;
          
          fTotalCounter++;
          histStore.fillTH1F("hEventInfo_Energy_PhotProcess_Total", wavelength);

          if (processName == "Scintillation") {
            fScintillationCounter++;
            histStore.fillTH1F("hEventInfo_Energy_PhotProcess_Scintillation", wavelength);
          }
          else
          if (processName == "Cerenkov") {
            fCerenkovCounter++;
            histStore.fillTH1F("hEventInfo_Energy_PhotProcess_Cerenkov", wavelength);
          }
          else {
            G4cout <<"WARNING::MSSteppingAction::UserSteppingAction Unknown creator process : " << secondaries->at(i)->GetCreatorProcess()->GetProcessName() << G4endl;
            fUnknownCounter++;
            histStore.fillTH1F("hEventInfo_Energy_PhotProcess_Unknown", wavelength);
          }
        }
      }
    }
  }
}

void MSSteppingAction::TakeActionsForPrimaryParticlesOnPlateSurface (const G4Step* step)
{
  const G4Track* track = step->GetTrack();
  // We are loooking at all particle coming from the gun, except opticalphoton.
  G4String ParticleName = track->GetDynamicParticle()->GetParticleDefinition()->GetParticleName();
  if (ParticleName == "opticalphoton") return;

  // We are acting at the plate surface
  G4StepPoint * point1  = step->GetPreStepPoint();
  G4StepPoint * point2 = step->GetPostStepPoint();

  G4ThreeVector position1 = point1->GetPosition();
  G4ThreeVector position2 = point2->GetPosition();

  G4TouchableHandle touch1 = point1->GetTouchableHandle();
  G4VPhysicalVolume* volume = touch1->GetVolume();
  G4String volumeName = volume->GetName();


  if (point1->GetStepStatus() == fGeomBoundary && volumeName.contains("Plate")) { 
    // We whant to fill coordinats in colimator coordinates system
    G4VPhysicalVolume* collimator = G4PhysicalVolumeStore::GetInstance()->GetVolume("Collimator");
    G4ThreeVector posToFill = position1 - collimator->GetTranslation();

    G4double electronE = point1->GetKineticEnergy();
 
    // G4cout  << "MSSteppingAction::TakeActionsForPrimaryParticlesOnPlateSurface (" << ParticleName << " " << electronE/keV 
    //         << ") Volume: " << volumeName << " PosToFill: " << posToFill << " Pos1: " << position1 << "Pos2: " << position2 << G4endl;

    histStore.fillTH3F("hEventInfo_PrimaryParticlesOnPlateSurface", posToFill.x(), posToFill.y(), electronE/keV);
  }
}