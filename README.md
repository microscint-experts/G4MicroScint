MicroScint detetctor monte-carlo model based on Geant4 toolkit
=============================

REQUIREMENTS
------------

* [Geant4](https://geant4.web.cern.ch/geant4/) -  toolkit for the simulation of the passage of particles through matter.
* [ROOT](https://root.cern.ch/) - is unavoidable in experimental particle physics.
* [gcc/g++ 4.9](https://gcc.gnu.org/) - compilers.
* [CMake >2.6]() - is an open-source, cross-platform family of tools designed to build, test and package software.

Please, if you run SSH session, do it with X11 forwarding enabled via
		
		$ ssh -Y YourUserName@lxplus.cern.ch

INSTALLATION
------------

Please make shure you are runing this code on the CERN lxplus nodes. 

		$ git clone https://gitlab.cern.ch/microscint-experts/G4MicroScint.git
		$ mkdir G4MicroScint_build && cd G4MicroScint_build
		$ source ../G4MicroScint/setG4Env.sh
		$ cmake  ../G4MicroScint
		$ make

RUN
------------

First of all check your eviroment variables. If you are runing on LXPLUS you can setup enviroment using setG4Env.sh script.

		$ source path/To/Source/Files/G4MicroScint/setG4Env.sh

To start program
		
		$ ./microscint

Also you can pass any config file 

		$ ./microscint -in /path/to/file/vis.mac

To set special output name for root file with histograms, specify -f option

		$ ./microscint -f myAwesomeFile.root

By default visualisation is off, but if you need one do
		
		$ ./microscint -v 1

After that you will get GUI/Terminal with Geant command line. Command are same as for standard Geant4. For example, to start datataking and get 1 event simply do

		$ /run/beamOn 1

Be careful, if you run with graphics. It takes a lot of time to visualise many events.

For exit do

		$ exit

Program will save all histograms


QUICK DEVELOPMENT START
-----------

There are a lot of exapmles on the AFS how to deal with Geant4.

		/afs/cern.ch/sw/lcg/external/geant4/10.1.p02/share/examples

For tutorials visit [Geant4 training page](http://geant4.cern.ch/support/training.shtml).

If you whant to switch Geant4 version or you need other architecture, look more [here](http://lcgapp.cern.ch/project/simu/geant4/gettingStarted.html).



The G4MicroScint Developer Team
