#ifndef WIN32
#include <unistd.h>
#endif

#include "MSPhysicsList.hh"
#include "MSDetectorConstruction.hh"
#include "MSActionInitialization.hh"

#include "HistogramStore.hh"
HistogramStore* HistogramStore::m_instance = 0;
#include "HistController.hh"
HistController* HistController::m_instance = 0;
static HistController& histCtrl = *HistController::getInstance();

#ifdef G4MULTITHREADED
#include "G4MTRunManager.hh"
#else
#include "G4RunManager.hh"
#endif

#include "G4UImanager.hh"
#include "G4UIsession.hh"
#include "G4UIterminal.hh"

#include "Randomize.hh"
#include <time.h>

#ifdef G4VIS_USE
#include "G4VisExecutive.hh"
#endif

#ifdef G4UI_USE
#include "G4UIExecutive.hh"
#endif

char* getCmdOption(char ** begin, char ** end, const std::string & option);
bool cmdOptionExists(char** begin, char** end, const std::string& option);

int main(int argc,char** argv) 
{
  if(cmdOptionExists(argv, argv+argc, "-h")) {
    std::cout << "-h option found! No help ready yet. Just belive in my magic ;)\n";
  }
  bool visualization = false;  
  char* chVisualization = getCmdOption(argv, argv + argc, "-v");
  if(chVisualization) {
    visualization = atoi(chVisualization);
  }
  G4String fileInName;
  char* chCtrlFileName = getCmdOption(argv, argv + argc, "-in");
  if(chCtrlFileName) {
    fileInName = chCtrlFileName;
  }
  G4String outFileName = "output.root";
  char* chOutFileName = getCmdOption(argv, argv + argc, "-f");
  if(chOutFileName) {
    outFileName = chOutFileName;
  }

  G4int seed = 123; // time(0)+getpid();
  char* chSeed = getCmdOption(argv, argv + argc, "-s");
  if(chSeed) {
    seed = atoi(chSeed);
  }

  #ifdef G4MULTITHREADED
    G4MTRunManager * runManager = new G4MTRunManager;
  #else
    // Choose the Random engine and set the seed
    G4Random::setTheEngine(new CLHEP::RanecuEngine);
    G4Random::setTheSeed(seed);
    G4RunManager * runManager = new G4RunManager;
  #endif

  // Set mandatory initialization classes
  // Detector construction
  MSDetectorConstruction* detector = new MSDetectorConstruction(); 
  runManager->SetUserInitialization(detector);
  
  // Physics list
  G4String physName = "QGSP_BERT_HP";
  runManager->SetUserInitialization(new MSPhysicsList(physName)); 
  // User action initialization
  runManager->SetUserInitialization(new MSActionInitialization(detector)); 
  
  runManager->Initialize();
  histCtrl.Initialize(detector);


  if(visualization) {
    #ifdef G4VIS_USE // Initialize visualization
      G4VisManager* visManager = new G4VisExecutive;
      visManager->Initialize();
    #endif

    G4UImanager * UImanager = G4UImanager::GetUIpointer();
    // Simple UI for Windows runs, no possibility of additional arguments
    if (fileInName.size()>3 && fileInName.contains("."))
    {
       G4String command = "/control/execute ";
       UImanager->ApplyCommand(command+fileInName);
    }
    else  {
      // Define (G)UI terminal for interactive mode
      #ifdef G4UI_USE
        G4UIExecutive * ui = new G4UIExecutive(argc,argv);
        #ifdef G4VIS_USE
          UImanager->ApplyCommand("/control/execute init.in");
        #endif
        if (ui->IsGUI())
          UImanager->ApplyCommand("/control/execute gui.mac");
        ui->SessionStart();
        delete ui;
      #endif
    }

    // job termination
    #ifdef G4VIS_USE
      delete visManager;
    #endif
  } else {
    G4UImanager * UImanager = G4UImanager::GetUIpointer();
    G4UIsession * session = new G4UIterminal(); 
    if (fileInName.size()>3 && fileInName.contains("."))
       UImanager->ExecuteMacroFile(fileInName);
    session->SessionStart();
  }

  delete runManager;

  histCtrl.DumpHistsToRootFile(outFileName.data());

  return 0;
}

char* getCmdOption(char ** begin, char ** end, const std::string & option)
{
    char ** itr = std::find(begin, end, option);
    if (itr != end && ++itr != end)
    {
        return *itr;
    }
    return 0;
}

bool cmdOptionExists(char** begin, char** end, const std::string& option)
{
    return std::find(begin, end, option) != end;
}
