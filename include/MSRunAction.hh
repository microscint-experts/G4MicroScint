#ifndef MSRunAction_h
#define MSRunAction_h 1

#include "globals.hh"

#include "G4UserRunAction.hh"

class G4Timer;
class G4Run;

class MSRunAction : public G4UserRunAction
{
  public:

    MSRunAction();
    virtual ~MSRunAction();

  public:

    virtual void BeginOfRunAction(const G4Run*);
    virtual void EndOfRunAction(const G4Run*);

  private:
    G4Timer* fTimer;

};

#endif
