#ifndef MSSteppingAction_h
#define MSSteppingAction_h 1

#include "G4UserSteppingAction.hh"
#include "globals.hh"

class MSSteppingAction : public G4UserSteppingAction
{
  public:
    MSSteppingAction();
    virtual ~MSSteppingAction();

    // method from the base class
    virtual void UserSteppingAction(const G4Step*);

  private:
    void ResetVars();
    void FillHists();
    
    void TakeActionsForBornOpticalPhoton(const G4Step* step);
    void TakeActionsForPrimaryParticlesOnPlateSurface(const G4Step* step);

  private:
  	G4int fEventNumber;
  	
  	G4int fTotalCounter;
  	G4int fUnknownCounter;
    G4int fScintillationCounter;
    G4int fCerenkovCounter;
};

#endif