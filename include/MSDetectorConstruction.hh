#ifndef MSDetectorConstruction_h
#define MSDetectorConstruction_h 1

class MSMaterials;

#include "globals.hh"
#include "G4ios.hh"

#include "G4RotationMatrix.hh"

#include <vector>
#include <string>

class G4Box;
class G4Tubs;
class G4EllipticalTube;

class G4LogicalVolume;
class G4VPhysicalVolume;

class G4Material;

class MSPhotonDetSD;

#include "G4VUserDetectorConstruction.hh"
#include "G4Cache.hh"

class MSDetectorConstruction : public G4VUserDetectorConstruction
{
  public:

    MSDetectorConstruction();
    virtual ~MSDetectorConstruction();

    virtual G4VPhysicalVolume* Construct();
    
    G4VPhysicalVolume* ConstructDetector();

    virtual void ConstructSDandField();


    // StringToRotationMatrix() converts a string "X90,Y45" into a
    // G4RotationMatrix.
    // This is an active rotation, in that the object is first rotated
    // around the parent's X axis by 90 degrees, then the object is
    // further rotated around the parent's Y axis by 45 degrees.
    // The return value points to a G4RotationMatrix on the heap, so
    // it is persistent. Angles are in degrees, can have decimals,
    // and can be negative. Axes are X, Y, Z.

    static G4RotationMatrix StringToRotationMatrix(G4String rotation);

    G4Material* FindMaterial(G4String);

    std::vector<G4VPhysicalVolume*> GetVectorOfAllPhysicalVolumes();
    std::vector<std::string> GetVectorOfNamesAllPhysicalVolumes();
    std::vector<std::string> GetVectorOfSiPMNames();

    G4ThreeVector GetCollimatorPositioning() const { return fCollimatorPositioning; }


  private:

    enum CoverType {NOCOVER=0, TAPE};

    MSMaterials*        fMaterials;

    G4LogicalVolume*    fLogVolumeWorld;
    G4VPhysicalVolume*  fPhysVolumeWorld;

    std::vector<G4VPhysicalVolume*> vPhysVolumesScint;
    std::vector<G4VPhysicalVolume*> vPhysVolSiPM;
    std::vector<G4VPhysicalVolume*> vPhysVolSupports;
 
    G4double           fWorldSizeX;
    G4double           fWorldSizeY;
    G4double           fWorldSizeZ;

    G4double           fMSplateX;
    G4double           fMSplateY;
    G4double           fMSplateZ;
    G4int              fPlateCoverType;

    G4double           fMSscintX;
    G4double           fMSscintY;
    G4double           fMSscintZ;
    G4double           fMSscintZshift;
    G4double           fMSScintGap;
    G4double           fMSScintWall;
    G4double           fSurfaceRoughness;

    G4double           fSiPMXsize;
    G4double           fSiPMYsize;
    G4double           fSiPMZsize;
    G4ThreeVector      fSiPMPositioning;

    G4double           fCollimatorZ;
    G4double           fCollimatorRIn;
    G4double           fCollimatorROut;
    G4double           fCollimatorOffset;
    G4ThreeVector      fCollimatorPositioning;

    G4int              fNScLanes;

    G4Cache<MSPhotonDetSD*> fSiPMSD;

  private:

    void CreatePlateSurface(G4VPhysicalVolume*);

    void CrateScintillator(G4LogicalVolume*, G4String aTag = "");
    void CreateScintSurface(G4VPhysicalVolume*);

    void CrateSiPM();
    void CrateCollimator();
    
    void UpdateGeometryParameters();

    G4int GetChannelNumber(G4String);
};

#endif
