#ifndef MSUserTrackInformation_h
#define MSUserTrackInformation_h 1

#include "G4VUserTrackInformation.hh"
#include "G4VPhysicalVolume.hh"


class MSUserTrackInformation : public G4VUserTrackInformation
{

  public:

    MSUserTrackInformation();
    virtual ~MSUserTrackInformation();
 
    const G4VPhysicalVolume* GetOriginalPhysicalVolume() const { return fOriginPhysicalVolume; }
    void SetOriginalPhysicalVolume (G4VPhysicalVolume* volume) { fOriginPhysicalVolume = volume; }


  private:
    G4VPhysicalVolume* fOriginPhysicalVolume;
};

#endif