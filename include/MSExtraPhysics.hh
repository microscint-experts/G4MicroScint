#ifndef MSExtraPhysics_h
#define MSExtraPhysics_h 1

#include "globals.hh"

#include "G4VPhysicsConstructor.hh"

class MSExtraPhysics : public G4VPhysicsConstructor
{
  public:

    MSExtraPhysics();
    virtual ~MSExtraPhysics();

    virtual void ConstructParticle();
    virtual void ConstructProcess();

};

#endif
