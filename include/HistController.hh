#ifndef __HIST_CONTROLLER_H__
#define __HIST_CONTROLLER_H__

#include "MSDetectorConstruction.hh"

#include <TFile.h>
#include <TH1.h>
#include <TString.h>

#include <iostream>
#include <string>

class HistController {
  public:
    static HistController* getInstance() {
      if (!m_instance)
        m_instance = new HistController();
      return m_instance;
    }

  private: 
    HistController();
    // HistController& operator=(HistController const&){}
    static HistController* m_instance;

  public:
  	~HistController();

  public:
    void Initialize(MSDetectorConstruction* detConstruction);
  	void DumpHistsToRootFile(std::string fileName);

  private:
  	void histInitialize_Statistics();
    void histInitialize_EventInfo();
  	void histInitialize_SiPMSD();

  public:
  	std::string fNameRootFile;

  public:
  	enum CutFlow { TOTAL_RUN=0, TOTAL_EV, SiPM, NCUT};

  private:
    MSDetectorConstruction* fDetector;

};

#endif