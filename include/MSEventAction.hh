#ifndef MSEventAction_h
#define MSEventAction_h 1

#include "globals.hh"

#include "G4UserEventAction.hh"

class G4Event;

class MSEventAction : public G4UserEventAction
{
  public:

    MSEventAction();
    virtual ~MSEventAction();

  public:
	virtual void BeginOfEventAction(const G4Event*);
	virtual void EndOfEventAction(const G4Event*);

  private:

};

#endif