#ifndef MSActionInitialization_h
#define MSActionInitialization_h 1

#include "G4VUserActionInitialization.hh"

class MSDetectorConstruction;
class G4GeneralParticleSource;

class MSActionInitialization : public G4VUserActionInitialization
{
  public:
    MSActionInitialization(MSDetectorConstruction*);
    virtual ~MSActionInitialization();

    virtual void BuildForMaster() const;
    virtual void Build() const;

  private:
    MSDetectorConstruction* fDetector;
};

#endif
