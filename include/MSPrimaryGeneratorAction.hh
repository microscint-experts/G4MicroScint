#ifndef MSPrimaryGeneratorAction_h
#define MSPrimaryGeneratorAction_h 1

#include "globals.hh"
#include "G4VUserPrimaryGeneratorAction.hh"

#include "G4AffineTransform.hh"

class G4ParticleGun;
class G4SingleParticleSource;

class G4Event;
class G4PhysicsTable;

class MSDetectorConstruction;

class MSPrimaryGeneratorAction : public G4VUserPrimaryGeneratorAction
{
  public:

    MSPrimaryGeneratorAction(MSDetectorConstruction*);
    virtual ~MSPrimaryGeneratorAction();

  public:
    virtual void GeneratePrimaries(G4Event*);

  private:
    void ParticleGun();
    void FixedGunDirection();
    void RandomGunDirectoin();


  protected:
    G4PhysicsTable* fIntegralTable;

  private:
    MSDetectorConstruction*   fDetector;
    G4ParticleGun*            fParticleGun;

    G4double fCosAlphaMin ;
    G4double fCosAlphaMax ;

};

#endif
