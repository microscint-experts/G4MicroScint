#ifndef MSPhotonDetSD_h
#define MSPhotonDetSD_h 1

#include "G4VSensitiveDetector.hh"
#include <map>

class G4Step;
class G4HCofThisEvent;
class TString;

class MSPhotonDetSD : public G4VSensitiveDetector
{
  public:
    MSPhotonDetSD(G4String );
    virtual ~MSPhotonDetSD();

    virtual void    Initialize(G4HCofThisEvent* );
    virtual G4bool  ProcessHits(G4Step* , G4TouchableHistory* );
    virtual void    EndOfEvent(G4HCofThisEvent*);

 public: 
    void ResetVectorPhysicalVolumes(std::vector<G4VPhysicalVolume*> vL);

  private:
    void ResetVars();

    void PhotonCounter_Total(G4Track*);
    void PhotonCounter_Processes(G4Track*);
    void PhotonCounter_Location(G4Track*);

  private:
    G4int fTotalCounter;

    // Processes
    G4int fUnknownProcessCounter;
    G4int fScintillationProcessCounter;
    G4int fCerenkovProcessCounter;  

  private:
    std::vector<G4VPhysicalVolume*> vDetPhysVolumes;
    std::vector<G4VPhysicalVolume*> vSiPMPhysVolumes;

    std::map<TString, int> fMapPhotFromCounters;

};

#endif
