#ifndef MSMaterials_h
#define MSMaterials_h 1

#include "globals.hh"
#include "G4Material.hh"
#include "G4NistManager.hh"

class MSMaterials
{
  public:

    virtual ~MSMaterials();
 
    static MSMaterials* GetInstance();

    G4Material* GetMaterial(const G4String);
 
  private:
 
    MSMaterials();

    void CreateMaterials();

  private:

    static MSMaterials* fInstance;

    G4NistManager*     fNistMan;

    G4Material*        fAir;

    G4Material*        fPMMA;
    G4Material*        fHBP;
    G4Material*        fEJ309;

};

#endif /*MSMaterials_h*/
