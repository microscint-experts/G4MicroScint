#ifndef MSPhysicsList_h
#define MSPhysicsList_h 1

class MSPhysicsListMessenger;

class MSStepMax;
class MSOpticalPhysics;

class G4VPhysicsConstructor;

#include "globals.hh"
#include "G4VModularPhysicsList.hh"


class MSPhysicsList: public G4VModularPhysicsList
{
  public:

    MSPhysicsList(G4String);
    virtual ~MSPhysicsList();

    void SetCuts();
    void SetCutForGamma(G4double);
    void SetCutForElectron(G4double);
    void SetCutForPositron(G4double);

    void SetStepMax(G4double);
    MSStepMax* GetStepMaxProcess();
    void AddStepMax();

    /// Remove specific physics from physics list.
    void RemoveFromPhysicsList(const G4String&);

    /// Make sure that the physics list is empty.
    void ClearPhysics();
    
    virtual void ConstructProcess();

    // Turn on or off the absorption process
    void SetAbsorption(G4bool);

    void SetNbOfPhotonsCerenkov(G4int);

    void SetVerbose(G4int);

private:

    G4double fCutForGamma;
    G4double fCutForElectron;
    G4double fCutForPositron;

    MSStepMax* fStepMaxProcess;

    MSOpticalPhysics* fOpticalPhysics;

    MSPhysicsListMessenger* fMessenger;

    G4bool fAbsorptionOn;
    
    G4VMPLData::G4PhysConstVectorData* fPhysicsVector;

};

#endif
