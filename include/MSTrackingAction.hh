#ifndef MSTrackingAction_h
#define MSTrackingAction_h 1

#include "G4UserTrackingAction.hh"

class MSTrackingAction : public G4UserTrackingAction {

  public:

    MSTrackingAction() { };
    virtual ~MSTrackingAction() { };

    virtual void PreUserTrackingAction(const G4Track*);
    virtual void PostUserTrackingAction(const G4Track*);
    
};

#endif
