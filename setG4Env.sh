#!/bin/bash

#LXPLUS G4 env setup script

source /afs/cern.ch/sw/lcg/contrib/gcc/4.9/x86_64-slc6/setup.sh
source /afs/cern.ch/sw/lcg/external/geant4/10.1.p02/x86_64-slc6-gcc49-opt-MT/CMake-setup.sh
source /afs/cern.ch/sw/lcg/app/releases/ROOT/6.06.08/x86_64-slc6-gcc49-opt/root/bin/thisroot.sh
export CXX=/afs/cern.ch/sw/lcg/contrib/gcc/4.9/x86_64-slc6/bin/g++
export CC=/afs/cern.ch/sw/lcg/contrib/gcc/4.9/x86_64-slc6/bin/gcc

